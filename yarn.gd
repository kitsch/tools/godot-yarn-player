class_name Yarn
extends Node


enum LINE_TYPE {
	COMMAND,
	CONDITIONAL,
	DIALOGUE,
	JUMP,
	OPTIONS
	SET,
}

var data := []


func get_nodes_by_tag(tag: String):
	var nodes := []

	for node in data:
		if node.has("tags") and Array(node.tags).has(tag):
			nodes.push_back(node)

	return nodes


func get_node_by_title(title: String):
	for node in data:
		if node.title == title:
			return node

	return null


func open(path: String):
	if path == "":
		return FAILED

	var file := File.new()
	var error := file.open(path, File.READ)

	if error != OK:
		printerr("Failed loading Yarn file: %s." % path)
		return FAILED

	data = []

	var raw_nodes_data = file.get_as_text().split("\n===")
	raw_nodes_data.remove(raw_nodes_data.size() - 1)

	for raw_node_data in raw_nodes_data:
		var node = {
			"lines": [],
			"raw": null,
			"tags": [],
		}
		var node_data = raw_node_data.split("\n---\n")

		# Process headers
		for header in node_data[0].split("\n"):
			header = header.strip_edges()

			if header == "":
				continue

			var pair = _parse_pair(header)
			if pair[0] == "tags":
				node.tags = pair[1].split(" ")
			else:
				node[pair[0]] = pair[1]

		node.raw = node_data[1].split("\n")
		var body = _process_body(node.raw)

		if not body:
			return

		for line in body:
			node.lines.push_back(line)

		data.push_back(node)

	file.close()
	return OK


func _is_option(value):
	return value.begins_with("[[") and value.find("|") > -1


# Multi-line structures

func _parse_conditional(lines: PoolStringArray):
	var condition
	var con_data = []
	var line_data = []

	var base_spaces := -1
	for real_line in lines:
		var spaces := 0
		while spaces < real_line.length() and real_line[spaces] == " ":
			spaces += 1

		if base_spaces == -1:
			base_spaces = spaces

		var line = real_line.strip_edges()

		if line.begins_with("<<if") and spaces == base_spaces:
			condition = line.substr(5, line.find_last(">>") - 5)
		elif line.begins_with("<<else") and spaces == base_spaces:
			con_data.push_back({
				"condition": condition,
				"lines": _process_body(line_data)
			})
			line_data = []

			if line == "<<else>>":
				condition = "true"
			else:
				condition = line.substr(9, line.find_last(">>") - 9)
		else:
			line_data.push_back(line)
			continue

	con_data.push_back({
		"condition": condition,
		"lines": _process_body(line_data)
	})

	return {
		"type": LINE_TYPE.CONDITIONAL,
		"data": con_data
	}


# Individual lines

func _parse_line(line: String):
	if line.begins_with("<<set "):
		return _parse_set(line)
	elif line.begins_with("<<"):
		return _parse_command(line)
	elif line.begins_with("[["):
		return _parse_jump(line)

	return _parse_dialogue(line)


func _parse_command(s: String):
	var data := s.substr(2, s.find_last(">>") - 2).split(" ")
	var command := data[0]
	data.remove(0)

	return {
		"type": LINE_TYPE.COMMAND,
		"command": command,
		"arguments": data
	}


func _parse_dialogue(s: String):
	# Detect line with no speaker
	if s.find(":") == -1 or s.find(":") > s.find(" "):
		return {
			"type": LINE_TYPE.DIALOGUE,
			"speaker": "",
			"text": s
		}

	var data = _parse_pair(s)

	return {
		"type": LINE_TYPE.DIALOGUE,
		"speaker": data[0],
		"text": data[1]
	}


func _parse_jump(s: String):
	var target := s.substr(2, s.find_last("]]") - 2)

	return {
		"type": LINE_TYPE.JUMP,
		"target": target
	}


func _parse_pair(s: String):
	var data = []
	for item in s.split(":", true, 1):
		data.push_back(item.lstrip(" "))
	return data


func _parse_set(s: String):
	var data: PoolStringArray = s.substr(2, s.find_last(">>") - 2).split(" ", true, 3)

	# Throw error in debug builds when setting variable without a $
	assert(data[1].begins_with("$"), "Yarn: Variable \"%s\" must be set with a preceding $.")

	return {
		"type": LINE_TYPE.SET,
		"variable": data[1].substr(1),
		"value": data[3]
	}


func _process_body(lines: Array):
	var line_id := 0
	var result := []

	while line_id < lines.size():
		var data: Dictionary
		var line = lines[line_id].strip_edges()

		if line.begins_with("//"):
			# Do nothing - it's a comment
			pass

		elif line.begins_with("<<if"):
			var start = line_id

			while !lines[line_id].begins_with("<<endif>>"):
				line_id += 1
				if line_id >= lines.size():
					printerr("Cannot find matching <<endif>> for <<if>> at line %s" % start)
					return FAILED

			data = _parse_conditional(Array(lines).slice(start, line_id - 1))

		elif _is_option(line):
			var options = []
			while (line_id < lines.size() and _is_option(lines[line_id])):
				var l = lines[line_id].strip_edges()
				var o = l.substr(2, l.find_last("]]") - 2).split("|", 1)
				options.push_back({
					"title": o[0],
					"lines": [_parse_jump("[[%s]]" % o[1])]
				})
				line_id += 1
			data = {
				"type": LINE_TYPE.OPTIONS,
				"options": options
			}

		elif line.begins_with("->"):
			var options := []
			while (line_id < lines.size()):
				var l = lines[line_id].strip_edges()
				if l.begins_with("->"):
					var d = {
						"lines": []
					}
					l = l.substr(3)
					d.title = l

					var if_start = l.find("<<if")
					if if_start > -1:
						var if_loc = if_start + 5
						d.condition = l.substr(if_loc, l.find_last(">>") - if_loc)
						d.title = l.substr(0, if_start - 1)

					options.push_back(d)
				elif l == "":
					break
				else:
					var d = _parse_line(l)
					options[options.size() - 1].lines.push_back(d)
				line_id += 1

			data = {
				"type": LINE_TYPE.OPTIONS,
				"options": options
			}

		elif line != "":
			data = _parse_line(line)

		if data:
			result.append(data)

		line_id += 1

	return result
