extends Node


const text_operators = [
	{ "tag": "neq", "replacement": "!=" },
	{ "tag": "eq", "replacement": "==" },
	{ "tag": "lte", "replacement": "<=" },
	{ "tag": "lt", "replacement": "<" },
	{ "tag": "gte", "replacement": ">=" },
	{ "tag": "gt", "replacement": ">" },
	{ "tag": "xor", "replacement": "^" },
]


func evaluate(input: String):
	# Replace variables
	var regex = RegEx.new()
	regex.compile("\\$\\w+")

	for result in regex.search_all(input):
		var id = result.get_string().substr(1)
		var value = _YarnStore.get(id)
		var start = input.substr(0, result.get_start())
		var end = input.substr(result.get_end(), -1)

		if typeof(value) == TYPE_BOOL:
			value = "true" if value else "false"

		input = "%s%s%s" % [start, value, end]

	# Replace text operators
	for op in text_operators:
		input = input.replace(op.tag + " ", op.replacement + " ")
		input = input.replace(" " + op.tag, " " + op.replacement)

	# Replace visited
	input = input.replace("visited(", "_YarnStore.get_visited(")

	var script := GDScript.new()
	script.set_source_code("func eval():\n\treturn " + input)
	script.reload()

	var reference := Reference.new()
	reference.set_script(script)

	return reference.eval()
