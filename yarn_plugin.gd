tool
extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("_YarnStore", "res://addons/godot-yarn-player/yarn_store.gd")


func _exit_tree():
	remove_autoload_singleton("_YarnStore")
