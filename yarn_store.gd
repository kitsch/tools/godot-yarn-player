extends Node


signal all_set(store)
signal set(key, value)

var _store = {
	"__visited": []
}


func get(key: String):
	return _store[key] if _store.has(key) else null


func get_all():
	return _store


func get_visited(title: String) -> bool:
	return _store.__visited.has(title)


func set(key: String, value):
	_store[key] = value
	emit_signal("set", key, value)


func set_all(store):
	_store = store
	emit_signal("all_set", store)


func set_visited(title: String):
	_store.__visited.append(title)
